FROM python:3.10-alpine

COPY ./requirements.txt /
RUN pip install --extra-index-url https://package-read:_UtxUoKFjoHGs9XJusBq@git-ce.rwth-aachen.de/api/v4/projects/2815/packages/pypi/simple wzl-utilities
RUN pip install -r requirements.txt

COPY ./src /src
WORKDIR /src

CMD ["python", "main.py"]