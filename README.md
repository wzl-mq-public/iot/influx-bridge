# MQTT2Influx Bridge

MQTT Client & Translational layer for InfluxDB  
If data packages are structured, according to the _SensOr Interfacing Language_, the database structure of an InfluxDB database can be derived automatically.

# Usage
Install all requirements via pip.
Create your own config.json from the sample file and ADD IT TO YOUR .gitignore to not upload your secrets!

# Deployment

For deployment of the Influx bridge the provided docker and docker compose files can be used.

# Acknowledgements

Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under Germany's Excellence Strategy – EXC-2023 Internet of Production – 390621612.
