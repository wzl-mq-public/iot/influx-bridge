import json
import datetime
from typing import Any
from wzl.utilities import root_logger

logger = root_logger.get(__name__)

class Message(object):

    def __init__(self, topic: str, raw_data: str):
        self._topic = topic
        self._raw_data = raw_data
        self._data = None

    def soil_to_influx(self) -> dict[str, Any]:
        if not self.is_soil_conform():
            raise ValueError('Can not convert to influx format. Message is not SOIL-conform.')

        if 'timestamp' not in self.data:
            self._data['timestamp'] = f'{datetime.datetime.utcnow().isoformat()}Z'

        logger.debug("### TAGS ###\n" + str(self.tags))

        if isinstance(self.value, list):
            fields = {
                f'{self.measurement_identifier.lower()}_{num}': (float(value) if type(value) == int else value)
                for num, value in enumerate(self.value)
            }

        else:
            fields = {
                self.measurement_identifier.lower(): float(self.value) if type(
                    self.value) == int else self.value
            }

        if self.is_soil_conform(strict=True):
            if len(self.data['dimension']) == 1:
                fields['dimension'] = int(self.data['dimension'][0])
            elif len(self.data['dimension']) > 1:
                fields['dimension'] = str(self.data['dimension'])
            else:
                assert len(self.data['dimension']) == 0
                fields['dimension'] = 'scalar'

            if 'covariance' in self.data:
                fields['covariance'] = str(self.data['covariance'])

        influx_format = {"measurement": self.measurement_identifier.lower(),
                 "tags": {"uuid": self.topic, **self.tags},
                 "fields": fields,
                 "time": self.data['timestamp'],
                 }

        return  influx_format

    @property
    def topic(self) -> str:
        return self._topic

    @property
    def measurement_identifier(self):
        if not self.is_soil_conform():
            raise ValueError('Message is not SOIL conform, can not return identifier of a measurement!')
        return self._topic.split('/')[-1][4:]

    @property
    def data(self) -> dict[str, Any]:
        if self._data is None:
            if not self.is_json():
                raise ValueError('Message can not be parsed, because the message does not have valid JSON-format!')
            self._data = json.loads(self._raw_data)
        return self._data

    @property
    def value(self) -> Any:
        if not self.is_soil_conform():
            raise ValueError('Message is not SOIL conform, can not return value!')
        return self.data["value"]

    @property
    def raw_data(self) -> str:
        return self._raw_data

    @property
    def tags(self) -> dict[str, Any]:
        tags = {}
        if not self.is_soil_conform():
            raise ValueError('Message is not SOIL conform, can not return identifier of a measurement!')
        # if "nonce" in self.data:
        #    tags = self.data["nonce"]  if isinstance(self.data["nonce"], dict) else {"nonce": self.data['nonce']}
        if "tags" in self.data:
            tags = self.data["tags"]  if isinstance(self.data["tags"], dict) else {"tags": self.data['tags']}
        if self.is_soil_conform(strict=True):
            tags['unit'] = self.data['unit']
            tags['datatype'] = self.data['datatype']
            if 'hash' in self.data:
                tags['hash'] = self.data['hash']
        return tags

    def __str__(self):
        return f'{self._topic}: {self._raw_data}'

    def is_json(self) -> bool:
        try:
            json.loads(self._raw_data)
            return True
        except json.JSONDecodeError:
            return False

    def is_soil_conform(self, strict: bool = False) -> bool:
        minimal_required_keys = ['value']
        strictly_required_keys = ['timestamp', 'unit', 'dimension', 'datatype']
        optional_keys = ['nonce', 'covariance', 'hash']

        # check the topic
        splitted_topic = self._topic.split('/')
        if splitted_topic[-1][:3] not in ["MEA", "VAR", "PAR"]:
            return False

        if strict:
            for part in splitted_topic[1:-1]:
                if part[:3] not in ['OBJ', 'COM']:
                    return False

        # check the message
        if not self.is_json():
            return False

        parsed_data = self.data
        for key in minimal_required_keys:
            if key not in parsed_data:
                return False

        if strict:
            for key in strictly_required_keys:
                if key not in parsed_data:
                    return False
            all_soil_keys = minimal_required_keys + strictly_required_keys + optional_keys
            for key in parsed_data:
                if key not in all_soil_keys:
                    return False
        return True
