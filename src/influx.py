import threading
import time
from queue import Queue, Empty
from typing import List

from influxdb import InfluxDBClient
from wzl.utilities import root_logger

logger = root_logger.get(__name__)


class InfluxWorkerThread(threading.Thread):

    def __init__(self, influx_client: InfluxDBClient, databases: List[str], queue: Queue, identifier: int,
                 max_block_length: int):
        threading.Thread.__init__(self)
        self._queue = queue
        self._influx_client = influx_client
        self._identifier = identifier
        self.databases = databases
        self._max_block_length = max_block_length
        self._running = True

    def stop(self):
        self._running = False

    def run(self):
        while self._running:
            if not self._queue.empty():
                points = {x: [] for x in self.databases}
                try:
                    for i in range(self._max_block_length):
                        point = self._queue.get(timeout=0.01)
                        points[point[0]].append(point[1])
                except Empty:
                    logger.debug(f'Worker {self._identifier}: Queue is empty.')
                except Exception as e:
                    logger.error(f'Worker {self._identifier}: {e}')

                for database in points:
                    if len(points[database]) > 0:
                        try:
                            logger.debug(
                                f'Worker {self._identifier} sent queue with length {len(points[database])} to {database}')
                            self._influx_client.write_points(points[database], database=database)
                        except Exception as e:
                            logger.error(e)
            else:
                time.sleep(0.1)
