import queue
import queue
import time
from enum import Enum
from typing import Union, List, Dict, Any

from influxdb import InfluxDBClient
from wzl.mqtt import MQTTSubscriber
from wzl.utilities import root_logger

from influx import InfluxWorkerThread
from message import Message

logger = root_logger.get(__name__)


class Mode(Enum):
    USERNAME = 0
    PREFIX = 1

    @classmethod
    def from_string(self, mode: str) -> 'Mode':
        return {'prefix': Mode.PREFIX, 'username': Mode.USERNAME}[mode]

    def __str__(self):
        return ['username', 'prefix'][self.value]


class Mapping(object):

    def __init__(self, topic: str, database: str, mode: Union[Mode, str]):
        self._topic = topic
        self._database = database
        self._mode = mode if isinstance(mode, Mode) else Mode.from_string(mode)

    @property
    def topic(self):
        return self._topic

    @property
    def database(self):
        return self._database

    @property
    def mode(self):
        return self._mode

    def __eq__(self, other: Union['Mapping', str]):
        """Compares two mappings for equality.

        As there is only one mapping per topic allowed, two mappings are considered equal, if the topic is the same.

        Args:
            other:

        Returns:

        """
        if isinstance(other, Mapping):
            return other.topic == self._topic
        else:
            assert isinstance(other, str)
            return other == self._topic

    def __str__(self):
        return f'Store messages with topic "{self._topic}" in database "{self._database}" with mode "{str(self._mode)}".'


class MQTT2InfluxDB(object):
    """
    Guess what...

    Steals a lot from mnt - ruuviiinflux (THANKS), but more general data input
    """

    def __init__(self, mappings: List[Mapping], influx_client: InfluxDBClient, mqtt_config: Dict[str, Any],
                 num_workers: int = 1):

        """
        Does, whatever init does.

        Args:
            topic_list (TYPE): DESCRIPTION.
            host (TYPE): DESCRIPTION.
            port (TYPE): DESCRIPTION.
            username (TYPE): DESCRIPTION.
            password (TYPE): DESCRIPTION.
            influx_client (TYPE): DESCRIPTION.

        Returns:
            None.

        """
        self._mappings = mappings
        self._influx_client = influx_client
        self._mqtt_subscriber = MQTTSubscriber(mqtt_config['username'])
        self._queue = queue.Queue(100000)

        # subscribe to all topic
        for mapping in self._mappings:
            if mapping.mode == Mode.USERNAME:
                self._mqtt_subscriber.subscribe(mapping.topic + "/#", 1)
            else:
                assert mapping.mode == Mode.PREFIX
                # self._mqtt_subscriber.subscribe(mapping.topic + "#", 1)
                # TODO implement
                pass

        databases = []
        for mapping in self._mappings:
            logger.info(mapping)
            if mapping.database not in databases:
                databases += [mapping.database]

        self._threads = [
            InfluxWorkerThread(
                influx_client=self._influx_client,
                databases=databases,
                queue=self._queue,
                identifier=i,
                max_block_length=50  # TODO get from config
            ) for i in range(num_workers)]
        for thread in self._threads:
            thread.start()

        self._mqtt_subscriber.set_callback('default', self.process_message)
        self._mqtt_subscriber.connect(mqtt_config['host'], mqtt_config['username'], mqtt_config['password'],
                                      mqtt_config['vhost'], mqtt_config['port'], ssl=mqtt_config['ssl'])

    def _get_database(self, topic: str):
        root_topic = topic.split('/')[0]
        for mapping in self._mappings:
            if mapping.topic == root_topic:
                return mapping.database
        raise ValueError('There is now mapping for the given topic.')

    def __del__(self):
        for thread in self._threads:
            thread.stop()

    def process_message(self, topic: str, payload):
        # start_time = time.perf_counter()

        topic_list = topic.split("/")
        if topic_list[-1][:3] in ["MEA", "VAR", "PAR"]:
            messages = []
            try:
                # check if the message itself contains multiple messages
                split_payload = payload.decode("utf-8").split("}{")
                for split in split_payload:
                    if len(messages) > 1:
                        if split[0] != "{":
                            split = f'{{{split}'  # double {{ is required to add { to the beginning of the string
                        if split[-1] != "}":
                            split = f'{split}}}'  # double }} is required to add } to the end of the string
                    logger.debug("### TOPIC ### \n" + str(topic))
                    logger.debug("### CONTENT ### \n" + split)
                    messages += [Message(topic, split)]

                for message in messages:
                    if message.is_soil_conform():
                        database = self._get_database(message.topic)
                        self._queue.put((database, message.soil_to_influx()))
                    else:
                        raise ValueError('Message is not a SOIL message, can not add the message to the database.')

            except Exception as e:
                logger.error(f'{e} - {topic}: {payload.decode("UTF-8")}')

        # logger.info(f'{(1000 * (time.perf_counter() - start_time))} ms')
