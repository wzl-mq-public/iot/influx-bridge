# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 09:26:39 2019

@author: sdr
"""

import toml as toml

CONFIG = toml.load('../config.toml')

from wzl.utilities import root_logger

root_logger.set_logging_level(level=CONFIG['application']['logging_level'])
logger = root_logger.get(__name__)

import influxdb

from mqtt import Mapping, MQTT2InfluxDB

if __name__ == "__main__":

    logger.info('Starting')

    influxdb_client = influxdb.InfluxDBClient(
        host=CONFIG['influx']['host'],
        port=CONFIG['influx']['port'],
        username=CONFIG['influx']['username'],
        password=CONFIG['influx']['password'],
        retries=0,
    )

    databases = [i['name'] for i in influxdb_client.get_list_database()]

    for mapping in CONFIG['mappings']:
        database_exists = False
        if mapping['database'].lower() in databases:
            logger.info(f'Database {mapping["database"]} found. Ready for use.')
            database_exists = True
            continue
        if not database_exists:
            influxdb_client.create_database(mapping['database'].lower())
            logger.info(f'Database {mapping["database"]} not found. Created new instance.')

    mappings = []
    for mapping in CONFIG['mappings']:
        for topic in mapping['topics']:
            if topic in mappings:
                raise ValueError(
                    f'There is already a mapping with topic {topic}. There is only one mapping per topic allowed.')
            mappings += [Mapping(topic, mapping['database'].lower(), mapping['mode'])]

    TRANSLATOR = MQTT2InfluxDB(
        mappings=mappings,
        influx_client=influxdb_client,
        mqtt_config=CONFIG['mqtt'],
        num_workers=CONFIG['application']['num_workers'],
    )
